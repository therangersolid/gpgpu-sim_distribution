################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/intersim2/routers/chaos_router.cpp \
../src/intersim2/routers/event_router.cpp \
../src/intersim2/routers/iq_router.cpp \
../src/intersim2/routers/router.cpp 

OBJS += \
./src/intersim2/routers/chaos_router.o \
./src/intersim2/routers/event_router.o \
./src/intersim2/routers/iq_router.o \
./src/intersim2/routers/router.o 

CPP_DEPS += \
./src/intersim2/routers/chaos_router.d \
./src/intersim2/routers/event_router.d \
./src/intersim2/routers/iq_router.d \
./src/intersim2/routers/router.d 


# Each subdirectory must supply rules for building sources it contributes
src/intersim2/routers/%.o: ../src/intersim2/routers/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -O0 -g3 -Wall -c -fmessage-length=0 -D_GLIBCXX_USE_NANOSLEEP -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


