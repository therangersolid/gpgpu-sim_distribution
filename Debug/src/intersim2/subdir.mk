################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/intersim2/batchtrafficmanager.cpp \
../src/intersim2/booksim_config.cpp \
../src/intersim2/buffer.cpp \
../src/intersim2/buffer_state.cpp \
../src/intersim2/config_utils.cpp \
../src/intersim2/credit.cpp \
../src/intersim2/flit.cpp \
../src/intersim2/flitchannel.cpp \
../src/intersim2/gputrafficmanager.cpp \
../src/intersim2/injection.cpp \
../src/intersim2/interconnect_interface.cpp \
../src/intersim2/intersim_config.cpp \
../src/intersim2/main.cpp \
../src/intersim2/misc_utils.cpp \
../src/intersim2/module.cpp \
../src/intersim2/outputset.cpp \
../src/intersim2/packet_reply_info.cpp \
../src/intersim2/rng_double_wrapper.cpp \
../src/intersim2/rng_wrapper.cpp \
../src/intersim2/routefunc.cpp \
../src/intersim2/stats.cpp \
../src/intersim2/traffic.cpp \
../src/intersim2/trafficmanager.cpp \
../src/intersim2/vc.cpp 

C_SRCS += \
../src/intersim2/rng-double.c \
../src/intersim2/rng.c 

OBJS += \
./src/intersim2/batchtrafficmanager.o \
./src/intersim2/booksim_config.o \
./src/intersim2/buffer.o \
./src/intersim2/buffer_state.o \
./src/intersim2/config_utils.o \
./src/intersim2/credit.o \
./src/intersim2/flit.o \
./src/intersim2/flitchannel.o \
./src/intersim2/gputrafficmanager.o \
./src/intersim2/injection.o \
./src/intersim2/interconnect_interface.o \
./src/intersim2/intersim_config.o \
./src/intersim2/main.o \
./src/intersim2/misc_utils.o \
./src/intersim2/module.o \
./src/intersim2/outputset.o \
./src/intersim2/packet_reply_info.o \
./src/intersim2/rng-double.o \
./src/intersim2/rng.o \
./src/intersim2/rng_double_wrapper.o \
./src/intersim2/rng_wrapper.o \
./src/intersim2/routefunc.o \
./src/intersim2/stats.o \
./src/intersim2/traffic.o \
./src/intersim2/trafficmanager.o \
./src/intersim2/vc.o 

C_DEPS += \
./src/intersim2/rng-double.d \
./src/intersim2/rng.d 

CPP_DEPS += \
./src/intersim2/batchtrafficmanager.d \
./src/intersim2/booksim_config.d \
./src/intersim2/buffer.d \
./src/intersim2/buffer_state.d \
./src/intersim2/config_utils.d \
./src/intersim2/credit.d \
./src/intersim2/flit.d \
./src/intersim2/flitchannel.d \
./src/intersim2/gputrafficmanager.d \
./src/intersim2/injection.d \
./src/intersim2/interconnect_interface.d \
./src/intersim2/intersim_config.d \
./src/intersim2/main.d \
./src/intersim2/misc_utils.d \
./src/intersim2/module.d \
./src/intersim2/outputset.d \
./src/intersim2/packet_reply_info.d \
./src/intersim2/rng_double_wrapper.d \
./src/intersim2/rng_wrapper.d \
./src/intersim2/routefunc.d \
./src/intersim2/stats.d \
./src/intersim2/traffic.d \
./src/intersim2/trafficmanager.d \
./src/intersim2/vc.d 


# Each subdirectory must supply rules for building sources it contributes
src/intersim2/%.o: ../src/intersim2/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -O0 -g3 -Wall -c -fmessage-length=0 -D_GLIBCXX_USE_NANOSLEEP -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/intersim2/%.o: ../src/intersim2/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


