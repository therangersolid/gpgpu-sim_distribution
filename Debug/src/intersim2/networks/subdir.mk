################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/intersim2/networks/anynet.cpp \
../src/intersim2/networks/cmesh.cpp \
../src/intersim2/networks/dragonfly.cpp \
../src/intersim2/networks/fattree.cpp \
../src/intersim2/networks/flatfly_onchip.cpp \
../src/intersim2/networks/fly.cpp \
../src/intersim2/networks/kncube.cpp \
../src/intersim2/networks/network.cpp \
../src/intersim2/networks/qtree.cpp \
../src/intersim2/networks/tree4.cpp 

OBJS += \
./src/intersim2/networks/anynet.o \
./src/intersim2/networks/cmesh.o \
./src/intersim2/networks/dragonfly.o \
./src/intersim2/networks/fattree.o \
./src/intersim2/networks/flatfly_onchip.o \
./src/intersim2/networks/fly.o \
./src/intersim2/networks/kncube.o \
./src/intersim2/networks/network.o \
./src/intersim2/networks/qtree.o \
./src/intersim2/networks/tree4.o 

CPP_DEPS += \
./src/intersim2/networks/anynet.d \
./src/intersim2/networks/cmesh.d \
./src/intersim2/networks/dragonfly.d \
./src/intersim2/networks/fattree.d \
./src/intersim2/networks/flatfly_onchip.d \
./src/intersim2/networks/fly.d \
./src/intersim2/networks/kncube.d \
./src/intersim2/networks/network.d \
./src/intersim2/networks/qtree.d \
./src/intersim2/networks/tree4.d 


# Each subdirectory must supply rules for building sources it contributes
src/intersim2/networks/%.o: ../src/intersim2/networks/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -O0 -g3 -Wall -c -fmessage-length=0 -D_GLIBCXX_USE_NANOSLEEP -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


