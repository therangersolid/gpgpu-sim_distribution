################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/intersim2/power/buffer_monitor.cpp \
../src/intersim2/power/power_module.cpp \
../src/intersim2/power/switch_monitor.cpp 

OBJS += \
./src/intersim2/power/buffer_monitor.o \
./src/intersim2/power/power_module.o \
./src/intersim2/power/switch_monitor.o 

CPP_DEPS += \
./src/intersim2/power/buffer_monitor.d \
./src/intersim2/power/power_module.d \
./src/intersim2/power/switch_monitor.d 


# Each subdirectory must supply rules for building sources it contributes
src/intersim2/power/%.o: ../src/intersim2/power/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -O0 -g3 -Wall -c -fmessage-length=0 -D_GLIBCXX_USE_NANOSLEEP -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


