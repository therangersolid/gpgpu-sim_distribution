################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/intersim2/allocators/allocator.cpp \
../src/intersim2/allocators/islip.cpp \
../src/intersim2/allocators/loa.cpp \
../src/intersim2/allocators/maxsize.cpp \
../src/intersim2/allocators/pim.cpp \
../src/intersim2/allocators/selalloc.cpp \
../src/intersim2/allocators/separable.cpp \
../src/intersim2/allocators/separable_input_first.cpp \
../src/intersim2/allocators/separable_output_first.cpp \
../src/intersim2/allocators/wavefront.cpp 

OBJS += \
./src/intersim2/allocators/allocator.o \
./src/intersim2/allocators/islip.o \
./src/intersim2/allocators/loa.o \
./src/intersim2/allocators/maxsize.o \
./src/intersim2/allocators/pim.o \
./src/intersim2/allocators/selalloc.o \
./src/intersim2/allocators/separable.o \
./src/intersim2/allocators/separable_input_first.o \
./src/intersim2/allocators/separable_output_first.o \
./src/intersim2/allocators/wavefront.o 

CPP_DEPS += \
./src/intersim2/allocators/allocator.d \
./src/intersim2/allocators/islip.d \
./src/intersim2/allocators/loa.d \
./src/intersim2/allocators/maxsize.d \
./src/intersim2/allocators/pim.d \
./src/intersim2/allocators/selalloc.d \
./src/intersim2/allocators/separable.d \
./src/intersim2/allocators/separable_input_first.d \
./src/intersim2/allocators/separable_output_first.d \
./src/intersim2/allocators/wavefront.d 


# Each subdirectory must supply rules for building sources it contributes
src/intersim2/allocators/%.o: ../src/intersim2/allocators/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -O0 -g3 -Wall -c -fmessage-length=0 -D_GLIBCXX_USE_NANOSLEEP -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


