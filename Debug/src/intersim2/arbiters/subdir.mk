################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/intersim2/arbiters/arbiter.cpp \
../src/intersim2/arbiters/matrix_arb.cpp \
../src/intersim2/arbiters/prio_arb.cpp \
../src/intersim2/arbiters/roundrobin_arb.cpp \
../src/intersim2/arbiters/tree_arb.cpp 

OBJS += \
./src/intersim2/arbiters/arbiter.o \
./src/intersim2/arbiters/matrix_arb.o \
./src/intersim2/arbiters/prio_arb.o \
./src/intersim2/arbiters/roundrobin_arb.o \
./src/intersim2/arbiters/tree_arb.o 

CPP_DEPS += \
./src/intersim2/arbiters/arbiter.d \
./src/intersim2/arbiters/matrix_arb.d \
./src/intersim2/arbiters/prio_arb.d \
./src/intersim2/arbiters/roundrobin_arb.d \
./src/intersim2/arbiters/tree_arb.d 


# Each subdirectory must supply rules for building sources it contributes
src/intersim2/arbiters/%.o: ../src/intersim2/arbiters/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -O0 -g3 -Wall -c -fmessage-length=0 -D_GLIBCXX_USE_NANOSLEEP -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


