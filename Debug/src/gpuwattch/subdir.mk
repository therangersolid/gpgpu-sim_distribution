################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/gpuwattch/XML_Parse.cc \
../src/gpuwattch/array.cc \
../src/gpuwattch/basic_components.cc \
../src/gpuwattch/core.cc \
../src/gpuwattch/gpgpu_sim_wrapper.cc \
../src/gpuwattch/interconnect.cc \
../src/gpuwattch/iocontrollers.cc \
../src/gpuwattch/logic.cc \
../src/gpuwattch/main.cc \
../src/gpuwattch/memoryctrl.cc \
../src/gpuwattch/noc.cc \
../src/gpuwattch/processor.cc \
../src/gpuwattch/sharedcache.cc \
../src/gpuwattch/technology_xeon_core.cc \
../src/gpuwattch/xmlParser.cc 

OBJS += \
./src/gpuwattch/XML_Parse.o \
./src/gpuwattch/array.o \
./src/gpuwattch/basic_components.o \
./src/gpuwattch/core.o \
./src/gpuwattch/gpgpu_sim_wrapper.o \
./src/gpuwattch/interconnect.o \
./src/gpuwattch/iocontrollers.o \
./src/gpuwattch/logic.o \
./src/gpuwattch/main.o \
./src/gpuwattch/memoryctrl.o \
./src/gpuwattch/noc.o \
./src/gpuwattch/processor.o \
./src/gpuwattch/sharedcache.o \
./src/gpuwattch/technology_xeon_core.o \
./src/gpuwattch/xmlParser.o 

CC_DEPS += \
./src/gpuwattch/XML_Parse.d \
./src/gpuwattch/array.d \
./src/gpuwattch/basic_components.d \
./src/gpuwattch/core.d \
./src/gpuwattch/gpgpu_sim_wrapper.d \
./src/gpuwattch/interconnect.d \
./src/gpuwattch/iocontrollers.d \
./src/gpuwattch/logic.d \
./src/gpuwattch/main.d \
./src/gpuwattch/memoryctrl.d \
./src/gpuwattch/noc.d \
./src/gpuwattch/processor.d \
./src/gpuwattch/sharedcache.d \
./src/gpuwattch/technology_xeon_core.d \
./src/gpuwattch/xmlParser.d 


# Each subdirectory must supply rules for building sources it contributes
src/gpuwattch/%.o: ../src/gpuwattch/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -O0 -g3 -Wall -c -fmessage-length=0 -D_GLIBCXX_USE_NANOSLEEP -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


