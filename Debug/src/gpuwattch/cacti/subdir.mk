################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/gpuwattch/cacti/Ucache.cc \
../src/gpuwattch/cacti/arbiter.cc \
../src/gpuwattch/cacti/area.cc \
../src/gpuwattch/cacti/bank.cc \
../src/gpuwattch/cacti/basic_circuit.cc \
../src/gpuwattch/cacti/cacti_interface.cc \
../src/gpuwattch/cacti/component.cc \
../src/gpuwattch/cacti/crossbar.cc \
../src/gpuwattch/cacti/decoder.cc \
../src/gpuwattch/cacti/highradix.cc \
../src/gpuwattch/cacti/htree2.cc \
../src/gpuwattch/cacti/io.cc \
../src/gpuwattch/cacti/main.cc \
../src/gpuwattch/cacti/mat.cc \
../src/gpuwattch/cacti/nuca.cc \
../src/gpuwattch/cacti/parameter.cc \
../src/gpuwattch/cacti/router.cc \
../src/gpuwattch/cacti/subarray.cc \
../src/gpuwattch/cacti/technology.cc \
../src/gpuwattch/cacti/uca.cc \
../src/gpuwattch/cacti/wire.cc 

OBJS += \
./src/gpuwattch/cacti/Ucache.o \
./src/gpuwattch/cacti/arbiter.o \
./src/gpuwattch/cacti/area.o \
./src/gpuwattch/cacti/bank.o \
./src/gpuwattch/cacti/basic_circuit.o \
./src/gpuwattch/cacti/cacti_interface.o \
./src/gpuwattch/cacti/component.o \
./src/gpuwattch/cacti/crossbar.o \
./src/gpuwattch/cacti/decoder.o \
./src/gpuwattch/cacti/highradix.o \
./src/gpuwattch/cacti/htree2.o \
./src/gpuwattch/cacti/io.o \
./src/gpuwattch/cacti/main.o \
./src/gpuwattch/cacti/mat.o \
./src/gpuwattch/cacti/nuca.o \
./src/gpuwattch/cacti/parameter.o \
./src/gpuwattch/cacti/router.o \
./src/gpuwattch/cacti/subarray.o \
./src/gpuwattch/cacti/technology.o \
./src/gpuwattch/cacti/uca.o \
./src/gpuwattch/cacti/wire.o 

CC_DEPS += \
./src/gpuwattch/cacti/Ucache.d \
./src/gpuwattch/cacti/arbiter.d \
./src/gpuwattch/cacti/area.d \
./src/gpuwattch/cacti/bank.d \
./src/gpuwattch/cacti/basic_circuit.d \
./src/gpuwattch/cacti/cacti_interface.d \
./src/gpuwattch/cacti/component.d \
./src/gpuwattch/cacti/crossbar.d \
./src/gpuwattch/cacti/decoder.d \
./src/gpuwattch/cacti/highradix.d \
./src/gpuwattch/cacti/htree2.d \
./src/gpuwattch/cacti/io.d \
./src/gpuwattch/cacti/main.d \
./src/gpuwattch/cacti/mat.d \
./src/gpuwattch/cacti/nuca.d \
./src/gpuwattch/cacti/parameter.d \
./src/gpuwattch/cacti/router.d \
./src/gpuwattch/cacti/subarray.d \
./src/gpuwattch/cacti/technology.d \
./src/gpuwattch/cacti/uca.d \
./src/gpuwattch/cacti/wire.d 


# Each subdirectory must supply rules for building sources it contributes
src/gpuwattch/cacti/%.o: ../src/gpuwattch/cacti/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -O0 -g3 -Wall -c -fmessage-length=0 -D_GLIBCXX_USE_NANOSLEEP -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


