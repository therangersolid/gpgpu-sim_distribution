################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/cuda-sim/cuda-sim.cc \
../src/cuda-sim/cuda_device_printf.cc \
../src/cuda-sim/cuda_device_runtime.cc \
../src/cuda-sim/instructions.cc \
../src/cuda-sim/memory.cc \
../src/cuda-sim/ptx-stats.cc \
../src/cuda-sim/ptx_ir.cc \
../src/cuda-sim/ptx_loader.cc \
../src/cuda-sim/ptx_parser.cc \
../src/cuda-sim/ptx_sim.cc 

OBJS += \
./src/cuda-sim/cuda-sim.o \
./src/cuda-sim/cuda_device_printf.o \
./src/cuda-sim/cuda_device_runtime.o \
./src/cuda-sim/instructions.o \
./src/cuda-sim/memory.o \
./src/cuda-sim/ptx-stats.o \
./src/cuda-sim/ptx_ir.o \
./src/cuda-sim/ptx_loader.o \
./src/cuda-sim/ptx_parser.o \
./src/cuda-sim/ptx_sim.o 

CC_DEPS += \
./src/cuda-sim/cuda-sim.d \
./src/cuda-sim/cuda_device_printf.d \
./src/cuda-sim/cuda_device_runtime.d \
./src/cuda-sim/instructions.d \
./src/cuda-sim/memory.d \
./src/cuda-sim/ptx-stats.d \
./src/cuda-sim/ptx_ir.d \
./src/cuda-sim/ptx_loader.d \
./src/cuda-sim/ptx_parser.d \
./src/cuda-sim/ptx_sim.d 


# Each subdirectory must supply rules for building sources it contributes
src/cuda-sim/%.o: ../src/cuda-sim/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -O0 -g3 -Wall -c -fmessage-length=0 -D_GLIBCXX_USE_NANOSLEEP -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


