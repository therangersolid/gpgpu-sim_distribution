################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/gpgpu-sim/addrdec.cc \
../src/gpgpu-sim/dram.cc \
../src/gpgpu-sim/dram_sched.cc \
../src/gpgpu-sim/gpu-cache.cc \
../src/gpgpu-sim/gpu-misc.cc \
../src/gpgpu-sim/gpu-sim.cc \
../src/gpgpu-sim/histogram.cc \
../src/gpgpu-sim/icnt_wrapper.cc \
../src/gpgpu-sim/l2cache.cc \
../src/gpgpu-sim/mem_fetch.cc \
../src/gpgpu-sim/mem_latency_stat.cc \
../src/gpgpu-sim/power_interface.cc \
../src/gpgpu-sim/power_stat.cc \
../src/gpgpu-sim/scoreboard.cc \
../src/gpgpu-sim/shader.cc \
../src/gpgpu-sim/stack.cc \
../src/gpgpu-sim/stat-tool.cc \
../src/gpgpu-sim/traffic_breakdown.cc \
../src/gpgpu-sim/visualizer.cc 

OBJS += \
./src/gpgpu-sim/addrdec.o \
./src/gpgpu-sim/dram.o \
./src/gpgpu-sim/dram_sched.o \
./src/gpgpu-sim/gpu-cache.o \
./src/gpgpu-sim/gpu-misc.o \
./src/gpgpu-sim/gpu-sim.o \
./src/gpgpu-sim/histogram.o \
./src/gpgpu-sim/icnt_wrapper.o \
./src/gpgpu-sim/l2cache.o \
./src/gpgpu-sim/mem_fetch.o \
./src/gpgpu-sim/mem_latency_stat.o \
./src/gpgpu-sim/power_interface.o \
./src/gpgpu-sim/power_stat.o \
./src/gpgpu-sim/scoreboard.o \
./src/gpgpu-sim/shader.o \
./src/gpgpu-sim/stack.o \
./src/gpgpu-sim/stat-tool.o \
./src/gpgpu-sim/traffic_breakdown.o \
./src/gpgpu-sim/visualizer.o 

CC_DEPS += \
./src/gpgpu-sim/addrdec.d \
./src/gpgpu-sim/dram.d \
./src/gpgpu-sim/dram_sched.d \
./src/gpgpu-sim/gpu-cache.d \
./src/gpgpu-sim/gpu-misc.d \
./src/gpgpu-sim/gpu-sim.d \
./src/gpgpu-sim/histogram.d \
./src/gpgpu-sim/icnt_wrapper.d \
./src/gpgpu-sim/l2cache.d \
./src/gpgpu-sim/mem_fetch.d \
./src/gpgpu-sim/mem_latency_stat.d \
./src/gpgpu-sim/power_interface.d \
./src/gpgpu-sim/power_stat.d \
./src/gpgpu-sim/scoreboard.d \
./src/gpgpu-sim/shader.d \
./src/gpgpu-sim/stack.d \
./src/gpgpu-sim/stat-tool.d \
./src/gpgpu-sim/traffic_breakdown.d \
./src/gpgpu-sim/visualizer.d 


# Each subdirectory must supply rules for building sources it contributes
src/gpgpu-sim/%.o: ../src/gpgpu-sim/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -O0 -g3 -Wall -c -fmessage-length=0 -D_GLIBCXX_USE_NANOSLEEP -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


