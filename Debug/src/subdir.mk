################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/abstract_hardware_model.cc \
../src/avf_cuda_exec.cc \
../src/avf_cuda_kernel.cc \
../src/avf_predictor.cc \
../src/avf_pvf_table.cc \
../src/debug.cc \
../src/gpgpusim_entrypoint.cc \
../src/option_parser.cc \
../src/statwrapper.cc \
../src/stream_manager.cc \
../src/trace.cc 

OBJS += \
./src/abstract_hardware_model.o \
./src/avf_cuda_exec.o \
./src/avf_cuda_kernel.o \
./src/avf_predictor.o \
./src/avf_pvf_table.o \
./src/debug.o \
./src/gpgpusim_entrypoint.o \
./src/option_parser.o \
./src/statwrapper.o \
./src/stream_manager.o \
./src/trace.o 

CC_DEPS += \
./src/abstract_hardware_model.d \
./src/avf_cuda_exec.d \
./src/avf_cuda_kernel.d \
./src/avf_predictor.d \
./src/avf_pvf_table.d \
./src/debug.d \
./src/gpgpusim_entrypoint.d \
./src/option_parser.d \
./src/statwrapper.d \
./src/stream_manager.d \
./src/trace.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -O0 -g3 -Wall -c -fmessage-length=0 -D_GLIBCXX_USE_NANOSLEEP -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


