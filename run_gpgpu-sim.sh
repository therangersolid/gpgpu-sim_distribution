#!/bin/bash
RUNDIR=$HOME/GeForceGTX1080Ti_rundir #GTX480_rundir GeForceGTX1080Ti_rundir
export PTX_SIM_DEBUG=0
cp -a $HOME/gpgpu-sim_distribution/configs/GeForceGTX1080Ti/ $RUNDIR #GTX480 GeForceGTX1080Ti
cd $RUNDIR
{ { $@; } > >(tee stdout.txt ); } 2> >( tee stderr.txt >&2 )
echo "GPGPU-Sim finished running \"$@\""
echo "Used rundir=$RUNDIR"

