/*
 * avf_predictor.h
 *
 *  Created on: Aug 17, 2018
 *      Author: gpgpu-sim
 *      All implemented at shader.cc
 */

#ifndef AVF_PREDICTOR_H_
#define AVF_PREDICTOR_H_
#include <vector>
#include "../cuda-sim/ptx_ir.h"
#include "../cuda-sim/ptx_sim.h"
#include "../abstract_hardware_model.h"


/**
 * This is for the name of the cuda function and get the register number
 */
void avfInitKernelRegister(const char* g_ptxinfo_kname, gpgpu_ptx_sim_info* g_ptxinfo_kinfo);
/**
 * This will initialize the variable, and the 2D vector.
 * Note for gtx480, we put the vector 64 for 'var' variable.
 * This is because it only has 64 register each thread. If
 * there is more register, you can increase this number as\
 * desired.
 */
void avfInitGridBlock(const char* kname,dim3* gridDim,dim3* blockDim);

/**
 * This will allocate the vector. Called in gpu-sim.cc at GPGPU-Sim uArch: core:%3d, cta:%2u initialized
 */
void avfAllocateVector();

/**
 * This class will be executed each gpu cycle.
 * The calculation for AVF will be done in exit_impl
 * in the instruction.cc
 */
void AvfPredictor_doCommit();

/**
 * Will print the message that 64 bit is unimplemented yet!
 */
void avf64Unimplemented(char* funcName);

/**
 * Will print the message that the AVF calculation for this
 * instruction is not implemented yet!
 */
void avfInstUnimplemented(char* funcName);

/**
 * Return the biggest value of a and b
 */
float maxFloat(float a, float b);

/**
 * This function is used to flatten out 3d function in cuda
 */
unsigned int flatten3Dto1Dcta(struct dim3 dimension);

unsigned int flatten3Dto1Dtid(struct dim3 dimension);

/**
 * Convert back flattened number to it's original
 */
struct dim3 convert1Dto3Dcta(unsigned int flatNum) ;

struct dim3 convert1Dto3Dtid(unsigned int flatNum);

/**
 * This is equivalent to class HvfPredictorObj, where it contains:
 * 1. Shader cycle of when it is used,
 * 2. Whether this is the destination or the source side register,
 * 3. The register's pvf.
 */
class AvfCheckPoint{
public:
    AvfCheckPoint(unsigned int regShaderCycles, bool regDest, float regPvf);
public:
	// To record that this register is used at which cycle
    uint64_t regShaderCycles;
	bool regDest;// True means destination, else source
	bool regWide;// True means has regPvfHigh. It uses two registers instead of one (64 bit operations)
    float regPvf; // ACE bit / total bit width
    float regPvfHigh; // high ace bit. Only used when using avfAddToContainer functions!

};

/**
 * Add the AvfCheckPoint object to avf_registerContainer in elegant manner!
 * Note:
 * pI->dst() = avf_checkPoint1; // Pvf's Representation
 * pI->src1() = avf_checkPoint2;
 * pI->src2() = avf_checkPoint3;
 * pI->src3() = avf_checkPoint4;
 *
 */
void avfAddToContainer(const ptx_instruction *pI, ptx_thread_info *thread,AvfCheckPoint *avf_checkPoint1,AvfCheckPoint *avf_checkPoint2,AvfCheckPoint *avf_checkPoint3,AvfCheckPoint *avf_checkPoint4);

/**
 * Add the AvfCheckPoint object to avf_registerContainer in elegant manner!
 */
void avfAddToContainer(const ptx_instruction *pI, ptx_thread_info *thread,AvfCheckPoint *avf_checkPoint1,AvfCheckPoint *avf_checkPoint2,AvfCheckPoint *avf_checkPoint3);

/**
 * Add the AvfCheckPoint object to avf_registerContainer in elegant manner!
 */
void avfAddToContainer(const ptx_instruction *pI, ptx_thread_info *thread,AvfCheckPoint *avf_checkPoint1,AvfCheckPoint *avf_checkPoint2);

int maxInteger(int a, int b);

/**
 * Print stats of the AVF
 */
void avfPrintStat();

/**
 * This function is for processing AVF for all the threads.
 * There are two points of entry:
 * 1. Those threads who execute exit. That thread will call this to calculate the AVF. (in instructions.cc at exit_impl)
 * 2. Those threads who does not execute exit because of the branch predication. (in shader.cc at avfPrintStat())
 */
void processAVF(unsigned int ctaNum, unsigned int threadNum, uint64_t avf_shader_cycles);

/**
 * This consist of number of thread and
 * 64 vector of registers, which inside of it tracks the source,
 * destination and pvf of register
 *
 * Number of CTA -> Number of thread -> Number of register -> AvfCheckPoint
 *
 * Important: Always put the source first! then the destination
 */
extern std::vector< std::vector< std::vector< std::vector<AvfCheckPoint*>* >* >* > avf_registerContainers;

extern std::vector<AvfCheckPoint*> avf_graph;

/**
 * True means that the avf is initialized (The avf_registerContainers). Therefore, do not initialize.
 * Else initialize them!.
 */
extern bool avfInit;

//// The current instruction count
//extern uint64_t avf_curIcount;

//// The current shader's cycles
//extern uint64_t avf_shader_cycles; // used in shader.cc

// Number of register available for each thread! set it in the init function
extern int numOfRegister;

// Number of thread available for each shader! set it in the init function
extern int numOfThread;

// Number of cycles before we pop out the avf
extern int avfSamplingInterval;

/**
 * Get the maxThreadsPerBlock from cuda_runtime_api.cc
 * This contains the total thread for an sm. In GTX480,
 * it is 512
 */
extern unsigned int totThreadPerBlock;

/**
 * Number of register per block (in a sm). In GTX480,
 * it is 32768 (Get it from the gpu configuration)
 */
extern unsigned int totNumOfRegisterPerBlock;

/**
 * get the number of sm in gpu from gpu-sim.cc
 * In GTX480, it is 15
 */
extern unsigned int totSIMTCluster;

/**
 * Sampling rate for shader's cycle
 */
extern unsigned int samplingRate;

/**
 * There are multiple level of debug:
 * >1.
 * >3. Show the Deletion of register at exit_impl
 * >5. Show the avf register's contains
 * >100. Instruction by instruction
 */
extern int avfDebug;

#endif /* AVF_PREDICTOR_H_ */
