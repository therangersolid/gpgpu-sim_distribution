/*
 * avf_predictor.h
 *
 *  Created on: Aug 17, 2018
 *      Author: gpgpu-sim
 *      All implemented at shader.cc
 */

#ifndef AVF_PREDICTOR_H_
#define AVF_PREDICTOR_H_
#include <vector>
#include <stdio.h>
#include "cuda-sim/ptx_ir.h"
#include "cuda-sim/ptx_sim.h"
#include "abstract_hardware_model.h"
#include "avf_pvf_table.h"
#include "avf_instruction_table.h"
#include "gpgpu-sim/mem_fetch.h"

#define function_unimplemented() (printf("File:%s , Line:%i , Function: %s is not implemented yet!\n",__FILE__, __LINE__, __func__))

/**
 * This is for the name of the cuda function and get the register number
 */
void avfInitKernelRegister(const char* g_ptxinfo_kname, gpgpu_ptx_sim_info* g_ptxinfo_kinfo);

/**
 * This will initialize the variable, and the 2D vector.
 * Note for gtx480, we put the vector 64 for 'var' variable.
 * This is because it only has 64 register each thread. If
 * there is more register, you can increase this number as\
 * desired.
 */
void avfInitGridBlock(const char* kname,dim3* gridDim,dim3* blockDim);


/**
 * This class will be executed each gpu cycle.
 * The calculation for AVF will be done in exit_impl
 * in the instruction.cc
 */
void AvfPredictor_doCommit();

/**
 * Will print the message that 64 bit is unimplemented yet!
 */
void avf64Unimplemented(char* funcName);

/**
 * Will print the message that the AVF calculation for this
 * instruction is not implemented yet!
 */
void avfInstUnimplemented(char* funcName);


///**
// * This function is used to flatten out 3d function in cuda
// */
//unsigned int flatten3Dto1Dcta(struct dim3* dimension);
//
//unsigned int flatten3Dto1Dtid(struct dim3* dimension);
//
///**
// * Convert back flattened number to it's original
// */
//void convert1Dto3Dcta(unsigned int flatNum, struct dim3* dimension) ;
//
//void convert1Dto3Dtid(unsigned int flatNum, struct dim3* dimension);

/**
 * Add the AvfCheckPoint object to avf_registerContainer in elegant manner!
 * Note:
 * pI->dst() = avf_checkPoint1; // Pvf's Representation
 * pI->src1() = avf_checkPoint2;
 * pI->src2() = avf_checkPoint3;
 * pI->src3() = avf_checkPoint4;
 *
 */
void avfAddToContainer(const ptx_instruction *pI, ptx_thread_info *thread,PvfTable *avf_checkPoint1,PvfTable *avf_checkPoint2,PvfTable *avf_checkPoint3,PvfTable *avf_checkPoint4);

/**
 * Add the AvfCheckPoint object to avf_registerContainer in elegant manner!
 */
void avfAddToContainer(const ptx_instruction *pI, ptx_thread_info *thread,PvfTable *avf_checkPoint1,PvfTable *avf_checkPoint2,PvfTable *avf_checkPoint3);

/**
 * Add the AvfCheckPoint object to avf_registerContainer in elegant manner!
 */
void avfAddToContainer(const ptx_instruction *pI, ptx_thread_info *thread,PvfTable *avf_checkPoint1,PvfTable *avf_checkPoint2);

void avfAddToContainerStore(const ptx_instruction *pI, ptx_thread_info *thread,
		PvfTable *avf_checkPoint1, PvfTable *avf_checkPoint2) ;
void avfAddToContainerLoad(const ptx_instruction *pI, ptx_thread_info *thread,
		PvfTable *avf_checkPoint1, PvfTable *avf_checkPoint2);

void avfInstructionAddToContainer(int shader_id, int warp_id, InstPvfTable* instPvfTable);

/**
 * Print stats of the AVF
 */
void avfPrintStat();

/**
 * This function is for processing AVF for all the threads.
 * There are two points of entry:
 * 1. Those threads who execute exit. That thread will call this to calculate the AVF. (in instructions.cc at exit_impl)
 * 2. Those threads who does not execute exit because of the branch predication. (in shader.cc at avfPrintStat())
 */
void processAVF(unsigned int ctaNum, unsigned int threadNum, uint64_t avf_shader_cycles);


/**
 * True means that the avf is initialized (The avf_registerContainers). Therefore, do not initialize.
 * Else initialize them!.
 */
extern bool avfInit;

extern int avfSamplingInterval;
extern unsigned int totNumOfRegisterPerBlock;
extern unsigned int totThreadPerBlock;
extern unsigned int totSIMTCluster;

extern int currentCudaCudaExecId;

/**
 * There are multiple level of debug:
 * >1. Show clock cycle status:
 * >100. Instruction by instruction
 */
extern int avfDebug;

#endif /* AVF_PREDICTOR_H_ */
