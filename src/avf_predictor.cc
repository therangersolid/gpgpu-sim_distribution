#include <stdio.h>
#include <vector>
#include <stdexcept>
#include <iostream>
#include "avf_predictor.h"
#include "avf_cuda_kernel.h"
#include "avf_cuda_exec.h"
#include "avf_pvf_table.h"
#include <string>
#include <pthread.h>

// For pthread
#include <unistd.h>
#include <syscall.h>

//For pointer
#include <stdint.h>
#include <inttypes.h>
#include "gpgpu-sim/gpu-sim.h"

//#define AVF_DEBUG
////////////////////////////////////////// GPU parameters

/**
 * Number of register per block (in a sm). In GTX480,
 * it is 32768 (Get it from the gpu configuration)
 */
unsigned int totNumOfRegisterPerBlock;

/**
 * Get the maxThreadsPerBlock from cuda_runtime_api.cc
 * This contains the total thread for an sm. In GTX480,
 * it is 512
 */
unsigned int totThreadPerBlock;

/**
 * get the number of sm in gpu from gpu-sim.cc
 * In GTX480, it is 15
 */
unsigned int totSIMTCluster;

// Number of cycles before we pop out the avf
int avfSamplingInterval;
long long print_avf = 0; // counter for sampling

// for enabling debug
int avfDebug = 0;

/// For current executions:
int numOfCTA = 0;
int numOfThread = 0;

int numOfWarp = 0;
int numOfShader = 1;

unsigned int blockWidth;
unsigned int blockHeight;
unsigned int blockDepth;
unsigned int gridWidth;
unsigned int gridHeight;
unsigned int gridDepth;

int avfFileCount = 0;

bool avfProgramInit = false;
pthread_mutex_t avf_lock;
/**
 * This will execute once per program executions
 */
void avfInitProgram() {
	printf("=====================Initializing SQLite3=================\n");
	pid_t threadId_os = syscall(SYS_gettid);
	printf("threadId of avfInitProgram %i\n", threadId_os);
	printf("sqlite3_threadsafe() = %i\n", sqlite3_threadsafe());
	if (!sqlite3_threadsafe()) {
		printf("\nSQLite is not thread safe! Aborting!\n");
		fflush(stdout);
		assert(0);
	}
	sqlite3_config(SQLITE_CONFIG_SERIALIZED);
	system("rm cuda_kernel.sqlite3");
	sqlite3* db;
	avf_database_open(&db);
	cuda_kernel_create(db);
	cuda_exec_create(db);
	avf_database_close(db);
	if (pthread_mutex_init(&avf_lock, NULL) != 0) {
		printf("\n mutex init has failed\n");
		assert(0);
	}
}

/**
 * This will execute 1st once per kernel function
 * From now on, The kernel register will be dynamically allocated (If there is some jumpy register or using the register bigger than allocated)
 */
void avfInitKernelRegister(const char* g_ptxinfo_kname,
		gpgpu_ptx_sim_info* g_ptxinfo_kinfo) {
	if (!avfProgramInit) {
		avfProgramInit = true;
		avfInitProgram();
	}
	printf("GPGPU-Sim AVF: avfInitKernelRegister ");
	printf(g_ptxinfo_kname);
	printf(" With register : %i\n", g_ptxinfo_kinfo->regs);
	CudaKernel cudaKernel;
	cudaKernel.kernel_name = g_ptxinfo_kname;
	// From now on, the number of register is set dynamically
	cudaKernel.numOfRegister = 0;
	sqlite3* db;
	avf_database_open(&db);
	cuda_kernel_insert(db, &cudaKernel);
	avf_database_close(db);
}

unsigned int flatten3Dto1Dcta(dim3 dimension) {
	return dimension.x + gridWidth * dimension.y
			+ gridWidth * gridHeight * dimension.z;
}

unsigned int flatten3Dto1Dtid(dim3 dimension) {
	return blockWidth * blockHeight * dimension.z + blockWidth * dimension.y
			+ dimension.x;
}

void convert1Dto3Dcta(unsigned int flatNum, struct dim3* dimension) {
	div_t outZ = div(flatNum, gridWidth * gridHeight);
	div_t outY = div(outZ.rem, gridWidth);
	dimension->z = outZ.quot;
	dimension->y = outY.quot;
	dimension->x = outY.rem;
}

void convert1Dto3Dtid(unsigned int flatNum, struct dim3* dimension) {
	div_t outZ = div(flatNum, blockWidth * blockHeight);
	div_t outY = div(outZ.rem, blockWidth);
	dimension->z = outZ.quot;
	dimension->y = outY.quot;
	dimension->x = outY.rem;
}
sqlite3* avfDatabase;
int currentCudaCudaExecId = 0;
/**
 * This will execute 2nd once per kernel execution
 */

void avfInitGridBlock(const char* kname, struct dim3* gridDim,
		struct dim3* blockDim) {
	pid_t threadId_os = syscall(SYS_gettid);
	printf("threadId of avfInitGridBlock %i\n", threadId_os);
	printf("=====================AVF initialized=================\n");
	print_avf = 0; // reset the counter
	printf("avfDebug = %i\n", avfDebug);
	printf("GPGPU-Sim AVF:avfInitGridBlock ");
	printf(kname);
	// Get the data from the database:
	CudaKernel cudaKernel;
	cudaKernel.kernel_name = kname;
	avf_database_open(&avfDatabase); // closed at print avf

	cuda_kernel_getid_by_kernel_name(avfDatabase, &cudaKernel);
	cuda_kernel_read(avfDatabase, &cudaKernel);

	if (cudaKernel.numOfRegister > totNumOfRegisterPerBlock) {
		cudaKernel.numOfRegister = totNumOfRegisterPerBlock; //Force the allocation if bigger than 64, force it to 64
	}
	printf(" With grid : %i, %i, %i; %i, %i, %i; %i\n", gridDim->x, gridDim->y,
			gridDim->z, blockDim->x, blockDim->y, blockDim->z,
			cudaKernel.numOfRegister);

	// Insert current execution status:
	CudaExec cudaExec;
	cudaExec.setCudaKernel(&cudaKernel);
	cudaExec.blockDim_x = blockDim->x;
	cudaExec.blockDim_y = blockDim->y;
	cudaExec.blockDim_z = blockDim->z;

	cudaExec.gridDim_x = gridDim->x;
	cudaExec.gridDim_y = gridDim->y;
	cudaExec.gridDim_z = gridDim->z;

	cuda_exec_insert(avfDatabase, &cudaExec);
	currentCudaCudaExecId = cudaExec.id;
	// The current shader's cycles
	blockWidth = blockDim->x;
	blockHeight = blockDim->y;
	blockDepth = blockDim->z;
	numOfThread = blockDim->x * blockDim->y * blockDim->z;

	gridWidth = gridDim->x;
	gridHeight = gridDim->y;
	gridDepth = gridDim->z;
	numOfCTA = gridDim->x * gridDim->y * gridDim->z;

	numOfWarp = 0;
	numOfShader = 1;

	// Generate the pvf table:
	printf("Generating pvf table:");
	pvf_table_create(avfDatabase, cudaExec.id);
	inst_pvf_table_create(avfDatabase, cudaExec.id);
	printf("Done!");
	sqlite3_exec(avfDatabase, "BEGIN TRANSACTION;", NULL, NULL, NULL);

}
/**
 * This class will be executed each gpu cycle.
 * The calculation for AVF will be done in exit_impl
 * in the instruction.cc
 */
//void AvfPredictor_doCommit() {
//}
void avf64Unimplemented(char* funcName) {
	printf("AVF for 64-bit is not implemented yet![");
	printf(funcName);
	printf("]\n");
}

void avfInstUnimplemented(char* funcName) {
	printf("AVF for [");
	printf(funcName);
	printf("] is not implemented yet!\n");
}

//template<class _Tp>
//_Tp maxValue(_Tp a, _Tp b) {
//	if (a > b) {
//		return a;
//	} else {
//		return b;
//	}
//}
//
//template<class _Tp>
//_Tp minValue(_Tp a, _Tp b) {
//	if (a > b) {
//		return b;
//	} else {
//		return a;
//	}
//}

void avfInstructionAddToContainer(int shader_id, int warp_id,
		InstPvfTable* instPvfTable) {
	if (
#ifdef AVF_DEBUG
	(avfDebug > 1)&&
#endif
	((instPvfTable->clock_cycle - print_avf) > avfSamplingInterval)) {
		printf("Clock cycle = %lli\n", instPvfTable->clock_cycle);
		print_avf = instPvfTable->clock_cycle;
	}
	instPvfTable->warp_id = warp_id;
	instPvfTable->shader_id = shader_id;
	CudaExec cudaExec;
	cudaExec.id = currentCudaCudaExecId;
	instPvfTable->setCudaExec(&cudaExec);
	// Warp id started from 0, Shader id started from 1
	inst_pvf_table_insert(avfDatabase, instPvfTable);

}

/**
 * avf_checkPoint1 are always destinations.
 */
void avfAddToContainer(const ptx_instruction *pI, ptx_thread_info *thread,
		PvfTable *avf_checkPoint1, PvfTable *avf_checkPoint2,
		PvfTable *avf_checkPoint3, PvfTable *avf_checkPoint4) {

	int threadId = flatten3Dto1Dtid(thread->get_tid());
	int blockId = flatten3Dto1Dcta(thread->get_ctaid());
//		pid_t threadId_os = syscall(SYS_gettid);
	/**
	 *  Deal with the source registers first. The logic here is that the source register and
	 *  the destination register maybe the same. Therefore, the source has to be placed first
	 *  (so that the previous avf_checkPoint destination has value)
	 */

	/**
	 *  Combine all the pvf which have the same cycles in the same registers
	 *  (By selecting the biggest). This means that the pvf is for an register
	 *  is coming from the same instruction. The biggest among those will be
	 *  selected.
	 */
	bool valid_checkPoint2 = false;
	bool valid_checkPoint3 = false;
	bool valid_checkPoint4 = false;
	PvfTable *avf_checkPoint2_64_ptr;
	PvfTable *avf_checkPoint3_64_ptr;
	PvfTable *avf_checkPoint4_64_ptr;
	// Only consider src1 if the avf_checkPoint2 is not NULL!
	if (avf_checkPoint2 != NULL) {
//		symbol* my_symbol = new symbol();
//		if (pI->src1().get_symbol()!=NULL){
//			memcpy(my_symbol,pI->src1().get_symbol(),sizeof(symbol));
//		}
		if (pI->src1().is_reg()
				&& (pI->src1().get_symbol()->arch_reg_num()
						< /*numOfRegister*/totNumOfRegisterPerBlock)) {
//			if (pI->src1().get_symbol()->arch_reg_num() >= numOfRegister) {
//				printf("Register surpassed the allocated register of %i with reg %i\n",
//						numOfRegister,pI->src1().get_symbol()->arch_reg_num());
//			}
#ifdef AVF_DEBUG
			if (avfDebug > 100) {
				printf(
						"Add src1, c%i,%i,%i;t%i,%i,%i to register %i with %9.6f\n",
						thread->get_ctaid().x, thread->get_ctaid().y,
						thread->get_ctaid().z, thread->get_tid().x,
						thread->get_tid().y, thread->get_tid().z,
						pI->src1().get_symbol()->arch_reg_num(),
						avf_checkPoint2->reg_pvf);
			}
#endif
			avf_checkPoint2->reg_id = pI->src1().get_symbol()->arch_reg_num();
			avf_checkPoint2->thread_id = threadId;
			avf_checkPoint2->block_id = blockId;
			avf_checkPoint2->shader_id = thread->get_hw_sid();
			CudaExec cudaExec;
			cudaExec.id = currentCudaCudaExecId;
			avf_checkPoint2->setCudaExec(&cudaExec);
			valid_checkPoint2 = true;

			//////////////////////
			if (avf_checkPoint2->regWide) {
#ifdef AVF_DEBUG
				if (avfDebug > 100) {
					printf(
							"Add upper bit src1, c%i,%i,%i;t%i,%i,%i to register %i with %9.6f\n",
							thread->get_ctaid().x, thread->get_ctaid().y,
							thread->get_ctaid().z, thread->get_tid().x,
							thread->get_tid().y, thread->get_tid().z,
							(pI->src1().get_symbol()->arch_reg_num()) + 1,
							avf_checkPoint2->regPvfHigh);
				}
#endif
				PvfTable avf_checkPoint2_64(avf_checkPoint2->clock_cycle, false,
						avf_checkPoint2->regPvfHigh);
				avf_checkPoint2_64.reg_id = avf_checkPoint2->reg_id + 1;
				avf_checkPoint2_64.thread_id = threadId;
				avf_checkPoint2_64.block_id = blockId;
				avf_checkPoint2_64.shader_id = thread->get_hw_sid();
				CudaExec cudaExec;
				cudaExec.id = currentCudaCudaExecId;
				avf_checkPoint2_64.setCudaExec(&cudaExec);
				avf_checkPoint2_64_ptr = &avf_checkPoint2_64;

			}
			//////////////////////
		}else
		if (pI->src1().is_vector()) {
			printf("vector_t Detected at cycle %llu! \n",
					avf_checkPoint2->clock_cycle);
		}
//		else
//		if (pI->src1().is_literal()) {
//			printf("is_literal Detected at cycle %llu! \n",
//					avf_checkPoint2->clock_cycle);
//		}
		fflush(stdout);

//		delete(my_symbol);
	}
	// Only consider src2 if the avf_checkPoint3 is not NULL!
	if (avf_checkPoint3 != NULL) {
		if (pI->src2().is_reg()
				&& (pI->src2().get_symbol()->arch_reg_num() < totNumOfRegisterPerBlock/*numOfRegister*/)) {
#ifdef AVF_DEBUG
			if (avfDebug > 100) {
				printf(
						"Add src2, c%i,%i,%i;t%i,%i,%i to register %i with %9.6f\n",
						thread->get_ctaid().x, thread->get_ctaid().y,
						thread->get_ctaid().z, thread->get_tid().x,
						thread->get_tid().y, thread->get_tid().z,
						pI->src2().get_symbol()->arch_reg_num(),
						avf_checkPoint3->reg_pvf);
			}
#endif
			//////////////////////////////////////////////////////////////////////////////////////////////////

			// Check with the 1st source if it's using the same register. If it's the same, combine
			if (valid_checkPoint2
					&& (pI->src1().get_symbol()->arch_reg_num()
							== pI->src2().get_symbol()->arch_reg_num())) {
				valid_checkPoint2 = false;
				avf_checkPoint3->reg_pvf = avf_checkPoint2->reg_pvf
						+ avf_checkPoint3->reg_pvf; // Possibility of combination
				if (avf_checkPoint2->regWide) {
					avf_checkPoint3->regWide = true;
					avf_checkPoint3->regPvfHigh = avf_checkPoint2->regPvfHigh
							+ avf_checkPoint3->regPvfHigh;
					if (avf_checkPoint3->regPvfHigh >= 1) {
						avf_checkPoint3->regPvfHigh = 1;
					}
				}
			}

			if (avf_checkPoint3->reg_pvf >= 1) {
				avf_checkPoint3->reg_pvf = 1;
			}
			avf_checkPoint3->reg_id = pI->src2().get_symbol()->arch_reg_num();
			avf_checkPoint3->thread_id = threadId;
			avf_checkPoint3->block_id = blockId;
			avf_checkPoint3->shader_id = thread->get_hw_sid();
			CudaExec cudaExec;
			cudaExec.id = currentCudaCudaExecId;
			avf_checkPoint3->setCudaExec(&cudaExec);
			valid_checkPoint3 = true;

			///////////////////////////////////////
			if (avf_checkPoint3->regWide) {
#ifdef AVF_DEBUG
				if (avfDebug > 100) {
					printf(
							"Add upper bit src2, c%i,%i,%i;t%i,%i,%i to register %i with %9.6f\n",
							thread->get_ctaid().x, thread->get_ctaid().y,
							thread->get_ctaid().z, thread->get_tid().x,
							thread->get_tid().y, thread->get_tid().z,
							(pI->src2().get_symbol()->arch_reg_num()) + 1,
							avf_checkPoint3->regPvfHigh);
				}
#endif
				PvfTable avf_checkPoint3_64(avf_checkPoint3->clock_cycle, false,
						avf_checkPoint3->regPvfHigh);
				avf_checkPoint3_64.reg_id = avf_checkPoint3->reg_id + 1;
				avf_checkPoint3_64.thread_id = threadId;
				avf_checkPoint3_64.block_id = blockId;
				avf_checkPoint3_64.shader_id = thread->get_hw_sid();
				CudaExec cudaExec;
				cudaExec.id = currentCudaCudaExecId;
				avf_checkPoint3_64.setCudaExec(&cudaExec);
				avf_checkPoint3_64_ptr = &avf_checkPoint3_64;
			}
			//////////////////////
		}
	}
	// Only consider src3 if the avf_checkPoint4 is not NULL!
	if (avf_checkPoint4 != NULL) {
		if (pI->src3().is_reg()
				&& (pI->src3().get_symbol()->arch_reg_num() < totNumOfRegisterPerBlock /*numOfRegister*/)) {
#ifdef AVF_DEBUG
			if (avfDebug > 100) {
				printf(
						"Add src3, c%i,%i,%i;t%i,%i,%i to register %i with %9.6f\n",
						thread->get_ctaid().x, thread->get_ctaid().y,
						thread->get_ctaid().z, thread->get_tid().x,
						thread->get_tid().y, thread->get_tid().z,
						pI->src3().get_symbol()->arch_reg_num(),
						avf_checkPoint4->reg_pvf);
			}
#endif
			//////////////
			// Check with the 1st source if it's using the same register. If it's the same, combine
			if (valid_checkPoint2
					&& (pI->src1().get_symbol()->arch_reg_num()
							== pI->src3().get_symbol()->arch_reg_num())) {
				valid_checkPoint2 = false;
				avf_checkPoint4->reg_pvf = avf_checkPoint2->reg_pvf
						+ avf_checkPoint4->reg_pvf;
				if (avf_checkPoint2->regWide) {
					avf_checkPoint4->regWide = true;
					avf_checkPoint4->regPvfHigh = avf_checkPoint2->regPvfHigh
							+ avf_checkPoint4->regPvfHigh;
					if (avf_checkPoint4->regPvfHigh >= 1) {
						avf_checkPoint4->regPvfHigh = 1;
					}
				}
			} else
			// Check with the 2nd source if it's using the same register. If it's the same, combine
			if (valid_checkPoint3
					&& (pI->src2().get_symbol()->arch_reg_num()
							== pI->src3().get_symbol()->arch_reg_num())) {
				valid_checkPoint3 = false;
				avf_checkPoint4->reg_pvf = avf_checkPoint3->reg_pvf
						+ avf_checkPoint4->reg_pvf;
				if (avf_checkPoint3->regWide) {
					avf_checkPoint4->regWide = true;
					avf_checkPoint4->regPvfHigh = avf_checkPoint3->regPvfHigh
							+ avf_checkPoint4->regPvfHigh;
					if (avf_checkPoint4->regPvfHigh >= 1) {
						avf_checkPoint4->regPvfHigh = 1;
					}
				}
			}

			if (avf_checkPoint4->reg_pvf >= 1) {
				avf_checkPoint4->reg_pvf = 1;
			}

			//////////////////////

			avf_checkPoint4->reg_id = pI->src3().get_symbol()->arch_reg_num();
			avf_checkPoint4->thread_id = threadId;
			avf_checkPoint4->block_id = blockId;
			avf_checkPoint4->shader_id = thread->get_hw_sid();
			CudaExec cudaExec;
			cudaExec.id = currentCudaCudaExecId;
			avf_checkPoint4->setCudaExec(&cudaExec);
			valid_checkPoint4 = true;
			//////////////////////
			if (avf_checkPoint4->regWide) {
#ifdef AVF_DEBUG
				if (avfDebug > 100) {
					printf(
							"Add upper bit src3, c%i,%i,%i;t%i,%i,%i to register %i with %9.6f\n",
							thread->get_ctaid().x, thread->get_ctaid().y,
							thread->get_ctaid().z, thread->get_tid().x,
							thread->get_tid().y, thread->get_tid().z,
							(pI->src3().get_symbol()->arch_reg_num()) + 1,
							avf_checkPoint4->regPvfHigh);
				}
#endif
				PvfTable avf_checkPoint4_64(avf_checkPoint4->clock_cycle, false,
						avf_checkPoint4->regPvfHigh);
				avf_checkPoint4_64.reg_id = avf_checkPoint4->reg_id + 1;
				avf_checkPoint4_64.thread_id = threadId;
				avf_checkPoint4_64.block_id = blockId;
				avf_checkPoint4_64.shader_id = thread->get_hw_sid();
				CudaExec cudaExec;
				cudaExec.id = currentCudaCudaExecId;
				avf_checkPoint4_64.setCudaExec(&cudaExec);
				avf_checkPoint4_64_ptr = &avf_checkPoint4_64;
			}
			//////////////////////
		}
	}

	// then the destination registers
	if (avf_checkPoint1 != NULL) {
		if (pI->dst().is_reg()
				&& (pI->dst().get_symbol()->arch_reg_num() < totNumOfRegisterPerBlock /*numOfRegister*/)) {
#ifdef AVF_DEBUG
			if (avfDebug > 100) {
				printf(
						"Add dst, c%i,%i,%i;t%i,%i,%i to register %i with %9.6f\n",
						thread->get_ctaid().x, thread->get_ctaid().y,
						thread->get_ctaid().z, thread->get_tid().x,
						thread->get_tid().y, thread->get_tid().z,
						pI->dst().get_symbol()->arch_reg_num(),
						avf_checkPoint1->reg_pvf);
			}
#endif

			avf_checkPoint1->reg_id = pI->dst().get_symbol()->arch_reg_num();
			avf_checkPoint1->thread_id = threadId;
			avf_checkPoint1->block_id = blockId;
			avf_checkPoint1->shader_id = thread->get_hw_sid();
			CudaExec cudaExec;
			cudaExec.id = currentCudaCudaExecId;
			avf_checkPoint1->setCudaExec(&cudaExec);
			pvf_table_insert(avfDatabase, avf_checkPoint1);
			//////////////////////
			if (avf_checkPoint1->regWide) {
#ifdef AVF_DEBUG
				if (avfDebug > 100) {
					printf(
							"Add upper bit dst, c%i,%i,%i;t%i,%i,%i to register %i with %9.6f\n",
							thread->get_ctaid().x, thread->get_ctaid().y,
							thread->get_ctaid().z, thread->get_tid().x,
							thread->get_tid().y, thread->get_tid().z,
							(pI->dst().get_symbol()->arch_reg_num()) + 1,
							avf_checkPoint1->regPvfHigh);
				}
#endif
				PvfTable avf_checkPoint1_64(avf_checkPoint1->clock_cycle, true,
						avf_checkPoint1->regPvfHigh);
				avf_checkPoint1_64.reg_id = avf_checkPoint1->reg_id + 1;
				avf_checkPoint1_64.thread_id = threadId;
				avf_checkPoint1_64.block_id = blockId;
				avf_checkPoint1_64.shader_id = thread->get_hw_sid();
				CudaExec cudaExec;
				cudaExec.id = currentCudaCudaExecId;
				avf_checkPoint1_64.setCudaExec(&cudaExec);
				pvf_table_insert(avfDatabase, &avf_checkPoint1_64);

			}
			//////////////////////
		}
	} else {
#ifdef AVF_DEBUG
		if (avfDebug > 100) {
			printf("null avf_checkPoint1");
		}
#endif
	}
	if (valid_checkPoint2) {
		pvf_table_insert(avfDatabase, avf_checkPoint2);
		if (avf_checkPoint2->regWide) {
			pvf_table_insert(avfDatabase, avf_checkPoint2_64_ptr);
		}
	}
	if (valid_checkPoint3) {
		pvf_table_insert(avfDatabase, avf_checkPoint3);
		if (avf_checkPoint3->regWide) {
			pvf_table_insert(avfDatabase, avf_checkPoint3_64_ptr);
		}
	}
	if (valid_checkPoint4) {
		pvf_table_insert(avfDatabase, avf_checkPoint4);
		if (avf_checkPoint4->regWide) {
			pvf_table_insert(avfDatabase, avf_checkPoint4_64_ptr);
		}
	}

	// For the instructions:
	uint64_t avf_shader_cycles =
			thread->get_core()->get_gpu()->getShaderStats()->shader_cycles[thread->get_hw_sid()];
	InstPvfTable instPvfTable(avf_shader_cycles, false, 1);
	avfInstructionAddToContainer(thread->get_hw_sid(), thread->get_hw_wid(),
			&instPvfTable);
}

void avfAddToContainer(const ptx_instruction *pI, ptx_thread_info *thread,
		PvfTable *avf_checkPoint1, PvfTable *avf_checkPoint2,
		PvfTable *avf_checkPoint3) {
	avfAddToContainer(pI, thread, avf_checkPoint1, avf_checkPoint2,
			avf_checkPoint3, NULL);
}

void avfAddToContainer(const ptx_instruction *pI, ptx_thread_info *thread,
		PvfTable *avf_checkPoint1, PvfTable *avf_checkPoint2) {
	avfAddToContainer(pI, thread, avf_checkPoint1, avf_checkPoint2, NULL);
}

void avfAddToContainerStore(const ptx_instruction *pI, ptx_thread_info *thread,
		PvfTable *avf_checkPoint1, PvfTable *avf_checkPoint2) {
	int threadId = flatten3Dto1Dtid(thread->get_tid());
	int blockId = flatten3Dto1Dcta(thread->get_ctaid());
	if (pI->src1().get_symbol()->is_reg_num_valid()
			&& (pI->src1().get_symbol()->arch_reg_num() < totNumOfRegisterPerBlock/*numOfRegister*/)) {

		avf_checkPoint2->reg_id = pI->src1().get_symbol()->arch_reg_num();
		avf_checkPoint2->thread_id = threadId;
		avf_checkPoint2->block_id = blockId;
		avf_checkPoint2->shader_id = thread->get_hw_sid();
		CudaExec cudaExec;
		cudaExec.id = currentCudaCudaExecId;
		avf_checkPoint2->setCudaExec(&cudaExec);
		pvf_table_insert(avfDatabase, avf_checkPoint2);
	}
	if (pI->dst().is_reg() && pI->dst().get_symbol()->is_reg_num_valid()
			&& (pI->dst().get_symbol()->arch_reg_num() < totNumOfRegisterPerBlock/*numOfRegister*/)) {
		avf_checkPoint1->reg_id = pI->src1().get_symbol()->arch_reg_num();
		avf_checkPoint1->thread_id = threadId;
		avf_checkPoint1->block_id = blockId;
		avf_checkPoint1->shader_id = thread->get_hw_sid();
		CudaExec cudaExec;
		cudaExec.id = currentCudaCudaExecId;
		avf_checkPoint1->setCudaExec(&cudaExec);
		pvf_table_insert(avfDatabase, avf_checkPoint1);
	}
}

void avfAddToContainerLoad(const ptx_instruction *pI, ptx_thread_info *thread,
		PvfTable *avf_checkPoint1, PvfTable *avf_checkPoint2) {
	int threadId = flatten3Dto1Dtid(thread->get_tid());
	int blockId = flatten3Dto1Dcta(thread->get_ctaid());
	if (pI->src1().get_symbol()->is_reg_num_valid()
			&& (pI->src1().get_symbol()->arch_reg_num() < totNumOfRegisterPerBlock/*numOfRegister*/)) {
		avf_checkPoint2->reg_id = pI->src1().get_symbol()->arch_reg_num();
		avf_checkPoint2->thread_id = threadId;
		avf_checkPoint2->block_id = blockId;
		avf_checkPoint2->shader_id = thread->get_hw_sid();
		CudaExec cudaExec;
		cudaExec.id = currentCudaCudaExecId;
		avf_checkPoint2->setCudaExec(&cudaExec);
		pvf_table_insert(avfDatabase, avf_checkPoint2);
	}
	if (pI->dst().is_reg() && pI->dst().get_symbol()->is_reg_num_valid()
			&& (pI->dst().get_symbol()->arch_reg_num() < totNumOfRegisterPerBlock /*numOfRegister*/)) {
		avf_checkPoint1->reg_id = pI->src1().get_symbol()->arch_reg_num();
		avf_checkPoint1->thread_id = threadId;
		avf_checkPoint1->block_id = blockId;
		avf_checkPoint1->shader_id = thread->get_hw_sid();
		CudaExec cudaExec;
		cudaExec.id = currentCudaCudaExecId;
		avf_checkPoint1->setCudaExec(&cudaExec);
		pvf_table_insert(avfDatabase, avf_checkPoint1);
	}
}
void avfPrintStat() {
	sqlite3_exec(avfDatabase, "END TRANSACTION;", NULL, NULL, NULL);
	printf("Print AVF Stats:\n");
	CudaExec cudaExec;
	cudaExec.id = currentCudaCudaExecId;
	cuda_exec_read(avfDatabase, &cudaExec);
	cudaExec.tot_wid = numOfWarp;
	cudaExec.tot_sid = numOfShader;
	cuda_exec_update(avfDatabase, &cudaExec);
	avf_database_close(avfDatabase);
	pid_t threadId_os = syscall(SYS_gettid);
	printf("threadId of avfPrintStat %i\n", threadId_os);
}
